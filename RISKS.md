# Team T5
### Documento dei rischi
<!-- TABLE_GENERATE_START -->
| NOME | CRITICITA/IMPATTO | DIFFICOLTA |
| ------------- | ------------- | ------------- |
|Definire log problematici| 5 | 4 |
|Creare il firewall| 4 | 4 |
|Creare la mappa| 2 | 5 |
|Trovare l'IP| 2 | 5 |
<!-- TABLE_GENERATE_END -->
