# Team T5
### Schema delle ore di lavoro
<!-- TABLE_GENERATE_START -->
| DATA | OGGETTO DELL'INCONTRO | DURATA |
| ------------- |  ------------- | ------------- |
|06/04/22| Lettura della traccia del progetto, creazione backlog e tabella dei rischi   | 5 ore |
|09/04/22 | Creazione diagramma dei casi d’uso, diagramma architetturale e mockup  | 4 ore |
<!-- TABLE_GENERATE_END -->
